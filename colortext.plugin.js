
var injectcolor = function(){};
var injectcolorinterval;

var injectcolortMainFunction = function(node) {
    if (!settingsCookie["bda-gs-7"]) return;

    node.querySelectorAll('.user-name').forEach(elem => {
        let color = elem.style.color;
        if (color === "rgb(255, 255, 255)") return;
        elem.closest(".message-group").querySelectorAll('.markup').forEach(elem => {
            if (elem.getAttribute("data-colour")) return;
            elem.setAttribute("data-colour", true);
            elem.style.setProperty("color", color);
        });
    });
};



injectcolor.prototype.getName = function(){ return "injectcolor Plugin"; };
injectcolor.prototype.getDescription = function(){ return "Maybe this works"; };
injectcolor.prototype.getVersion = function(){ return "0.1A"; };
injectcolor.prototype.getAuthor = function(){ return "Ordo"; };



injectcolor.prototype.start = function(){
	injectcolorInterval = setInterval(injectcolorMainFunction, 100);	//change this lower if needed
};

injectcolor.prototype.stop = function(){
	clearInterval(injectcolorInterval);
};

injectcolor.prototype.observer = function({addedNodes, removedNodes}) {
	if(addedNodes && addedNodes.length && addedNodes[0].classList && addedNodes[0].classList.contains('messages-wrapper')) {
		injectcolorMainFunction();
	}
};
injectcolor.prototype.getSettingsPanel = function(){ return "There are no settings here."; };



